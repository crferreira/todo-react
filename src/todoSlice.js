import { createSlice } from "@reduxjs/toolkit";

let idGenerator = 0;

const idItemAttribute = (id) => `item-${id}`;

function addItemInterno(state, item) {
  const objetoNovoItem = { ...item };

  objetoNovoItem.id = idGenerator++;

  state.idsItens = [objetoNovoItem.id, ...state.idsItens];
  state.itens[idItemAttribute(objetoNovoItem.id)] = objetoNovoItem;
}

function removeItemInterno(state, idItem) {
  const idAttribute = idItemAttribute(idItem);

  delete state.itens[idAttribute];

  state.idsItens = state.idsItens.filter((id) => id !== idItem);

  if (state.idsItens.length === 0) state.selecionarTodos = false;
}

export const filtros = {
  EXIBIR_TODOS: "TODOS",
  EXIBIR_A_FAZER: "A FAZER",
  EXIBIR_CONCLUIDOS: "CONCLUIDOS",
};

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    idsItens: [],
    itens: {},
    novoItem: "",
    filtro: filtros.EXIBIR_TODOS,
    selecionarTodos: false,
  },
  reducers: {
    initItens: (state, action) => {
      if (state.idsItens.length === 0) {
        action.payload.forEach((i) => addItemInterno(state, i));
      }
    },
    addItem: (state, action) => {
      addItemInterno(state, action.payload);
    },
    updateNovoItem: (state, action) => {
      state.novoItem = action.payload;
    },

    updateItemStatus: (state, action) => {
      const checked = action.payload.checked;

      state.itens[idItemAttribute(action.payload.id)].checked = checked;

      if (!checked) {
        state.selecionarTodos = false;
      } else {
        console.log(state.itens);
        const itensConcluidos = state.idsItens
          .map((id) => state.itens[idItemAttribute(id)])
          .filter((i) => i.checked);

        state.selecionarTodos =
          itensConcluidos.length > 0 &&
          itensConcluidos.length === state.idsItens.length;
      }
    },

    removeItem: (state, action) => {
      removeItemInterno(state, action.payload);
    },

    removeItensConcluidos: (state) => {
      state.idsItens
        .filter((id) => state.itens[idItemAttribute(id)].checked)
        .forEach((id) => removeItemInterno(state, id));
    },

    updateSelecionarTodos: (state, action) => {
      Object.values(state.itens).forEach((i) => (i.checked = action.payload));
      state.selecionarTodos = action.payload;
    },
    updateFiltro: (state, action) => {
      state.filtro = action.payload;
    },
  },
});

export const selectIdsItens = (state) => state.todo.idsItens;
export const selectNovoItem = (state) => state.todo.novoItem;
export const selectItem = (id) => (state) =>
  state.todo.itens[idItemAttribute(id)];
export const selectSelecionarTodos = (state) => state.todo.selecionarTodos;

export const selectFiltro = (state) => state.todo.filtro;
export const selectObjetosItens = (state) =>
  state.todo.idsItens.map((id) => state.todo.itens[idItemAttribute(id)]);

export const selectNumeroItensAFazer = (state) =>
  state.todo.idsItens.filter(
    (id) => !state.todo.itens[idItemAttribute(id)].checked
  ).length;

export const {
  updateNovoItem,
  addItem,
  updateItemStatus,
  removeItem,
  updateSelecionarTodos,
  initItens,
  updateFiltro,
  removeItensConcluidos,
} = todoSlice.actions;
