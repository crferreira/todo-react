import { Stack } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import { Item } from "./Item";
import {
  filtros,
  selectFiltro,
  selectIdsItens,
  selectObjetosItens,
} from "./todoSlice";

export const Itens = React.memo(() => {
  const todosIds = useSelector(selectIdsItens);
  const filtro = useSelector(selectFiltro);
  const objetosItens = useSelector(selectObjetosItens);

  let idsFiltrados = todosIds;

  if (filtro === filtros.EXIBIR_A_FAZER) {
    idsFiltrados = objetosItens.filter((i) => !i.checked).map((i) => i.id);
  } else if (filtro === filtros.EXIBIR_CONCLUIDOS) {
    idsFiltrados = objetosItens.filter((i) => i.checked).map((i) => i.id);
  }

  return (
    <Stack sx={{ display: "flex" }}>
      {idsFiltrados.map((id) => (
        <Item key={id} id={id} />
      ))}
    </Stack>
  );
});
