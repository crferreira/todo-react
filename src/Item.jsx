import { Stack, Typography, Checkbox, IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { removeItem, selectItem, updateItemStatus } from "./todoSlice";

export const Item = React.memo(
  ({ id, onItemStatusChange, onItemRemoveButtomClick }) => {
    const { checked, label } = useSelector(selectItem(id)) || {};
    const dispatch = useDispatch();

    const handleItemStatusChange = (e) =>
      dispatch(updateItemStatus({ id, checked: e.target.checked }));

    const handleRemoveButtomClick = () => dispatch(removeItem(id));

    return (
      <Stack direction="row" spacing={1} alignItems="center">
        <Checkbox checked={checked} onChange={handleItemStatusChange} />
        <Typography
          variant="body1"
          flexGrow={1}
          sx={{
            textDecoration: checked ? "line-through" : "none",
          }}
        >
          {label}
        </Typography>
        <IconButton onClick={handleRemoveButtomClick}>
          <DeleteIcon />
        </IconButton>
      </Stack>
    );
  }
);
