import {
  Box,
  Button,
  Stack,
  Typography,
  Checkbox,
  Divider,
  ToggleButtonGroup,
  ToggleButton,
} from "@mui/material";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import React, { useEffect } from "react";
import { NovoItemInput } from "./NovoItemInput";
import { Itens } from "./Itens";
import { useDispatch, useSelector } from "react-redux";
import {
  initItens,
  selectFiltro,
  selectSelecionarTodos,
  updateFiltro,
  updateSelecionarTodos,
  filtros,
  removeItensConcluidos,
  selectNumeroItensAFazer,
} from "./todoSlice";

const css = {
  main: {
    minWidth: 1,
    display: "flex",
    justifyContent: "center",
    mt: 2,
    "& .conteudo": {
      width: 800,
    },
  },
};

const itemsIniciais = [...Array(10).keys()].map((i) => ({
  label: `Item ${i}`,
  checked: false,
}));

export default function TodoApp() {
  const filtro = useSelector(selectFiltro);
  const selecionarTodos = useSelector(selectSelecionarTodos);
  const numeroItensAFazer = useSelector(selectNumeroItensAFazer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(initItens(itemsIniciais));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleSelecionarTodos(e) {
    dispatch(updateSelecionarTodos(e.target.checked));
  }

  const removerConcluidos = () => dispatch(removeItensConcluidos());

  function handleFiltroItems(e) {
    dispatch(updateFiltro(e.target.value));
  }

  return (
    <Box sx={css.main}>
      <Stack className="conteudo" spacing={2}>
        <Typography variant="h3">TODO</Typography>
        <NovoItemInput />
        <Itens />
        <Divider sx={{ my: 2 }} />
        <Stack direction="row" alignItems="center">
          <Checkbox checked={selecionarTodos} onClick={handleSelecionarTodos} />
          <Typography variant="body1" flexGrow="1">
            Selecionar todos
          </Typography>
          <Typography variant="subtitle2">
            {numeroItensAFazer} itens restando
          </Typography>
        </Stack>
        <Divider sx={{ my: 2 }} />
        <Stack direction="row" justifyContent="space-between">
          <ToggleButtonGroup value={filtro} onClick={handleFiltroItems}>
            <ToggleButton value={filtros.EXIBIR_TODOS}>Todos</ToggleButton>
            <ToggleButton value={filtros.EXIBIR_A_FAZER}>A fazer</ToggleButton>
            <ToggleButton value={filtros.EXIBIR_CONCLUIDOS}>
              Concluídos
            </ToggleButton>
          </ToggleButtonGroup>
          <Button onClick={removerConcluidos}>Remover concluídos</Button>
        </Stack>
      </Stack>
    </Box>
  );
}
