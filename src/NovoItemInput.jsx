import { TextField } from "@mui/material";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateNovoItem, addItem, selectNovoItem } from "./todoSlice";

export const NovoItemInput = React.memo(() => {
  const novoItem = useSelector(selectNovoItem);
  const dispatch = useDispatch();

  const handleNewItemChange = (e) => {
    dispatch(updateNovoItem(e.target.value));
  };

  const handleNewItemKeyDown = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      if (novoItem.trim() !== "") {
        dispatch(addItem({ label: novoItem, checked: false }));
        dispatch(updateNovoItem(""));
      }
    }
  };

  return (
    <TextField
      sx={{ mt: 1 }}
      label="Próxima tarefa"
      value={novoItem}
      onChange={handleNewItemChange}
      helperText="pressione ENTER para inserir"
      onKeyDown={handleNewItemKeyDown}
    />
  );
});
